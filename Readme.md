
Gameboy Joypad test project
===========================

Using [GBDK 2020](https://github.com/gbdk-2020/gbdk-2020), sound fx by [CBT-FX](https://github.com/datguywitha3ds/CBT-FX) Visual Studio Code project for basic input testing. Background tiles/sprites and map designed in [GBTD & GBMB](https://github.com/gbdk-2020/GBTD_GBMB)

![Joypad test](https://gitlab.com/michal.ursiny/gameboyexperiments/-/raw/main/screenshot.jpeg)