#include <gb/gb.h>
#include <gb/sgb.h>
#include <string.h>
#include <gbdk/console.h>
#include <stdio.h>
#include <gbdk/font.h>
#include "../gfx/sprites.c"
#include "../gfx/background_tiles.c"
#include "../gfx/background.c"
#include "../include/cbtfx.h"
#include "../include/hUGEDriver.h"
#include "../include/sfx/SFX_00.h"
#include "../include/sfx/SFX_01.h"
#include "../include/sfx/SFX_02.h"

#define SPRITE_UP_INDEX 0
#define SPRITE_UP_RIGHT_INDEX 1
#define SPRITE_RIGHT_INDEX 2
#define SPRITE_DOWN_RIGHT_INDEX 3
#define SPRITE_DOWN_INDEX 4
#define SPRITE_DOWN_LEFT_INDEX 5
#define SPRITE_LEFT_INDEX 6
#define SPRITE_UP_LEFT_INDEX 7
#define SPRITE_BUTTON_A_INDEX 8
#define SPRITE_BUTTON_B_INDEX 9
#define SPRITE_BUTTON_SEL_INDEX 10
#define SPRITE_BUTTON_STA_INDEX 11
#define SPRITE_NUMBERS 12

uint8_t getPositionXFromTile(uint8_t tile_x) {
   return 48 + tile_x * 8 + 8;
}

uint8_t getPositionYFromTile(uint8_t tile_y) {
   return 64 + tile_y * 8 + 16;
}

BOOLEAN isSpriteHidden(uint8_t sprite) {
   OAM_item_t* sprite_item = &shadow_OAM[sprite];
   return sprite_item->y == 0;
}

void hideExcept(uint8_t exception) {
   for (int i = 0; i < SPRITE_NUMBERS; i++) {
      if (i != exception) {
         hide_sprite(i);
      }
   }
}

void testAndDisplaySpriteDpad(uint8_t pressedState) {
 
   // diagonal
   if ((pressedState & J_UP) && (pressedState & J_RIGHT)) {
      if (isSpriteHidden(SPRITE_UP_RIGHT_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_01;
      }      
      move_sprite(SPRITE_UP_RIGHT_INDEX, getPositionXFromTile(2), getPositionYFromTile(0)); // 2 * 8 + 8, 1 * 8 + 8
      hideExcept(SPRITE_UP_RIGHT_INDEX);
      return;
   } else {
      hide_sprite(SPRITE_UP_RIGHT_INDEX);
   }
   if ((pressedState & J_RIGHT) && (pressedState & J_DOWN)) {
      if (isSpriteHidden(SPRITE_DOWN_RIGHT_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_01;
      }      
      move_sprite(SPRITE_DOWN_RIGHT_INDEX, getPositionXFromTile(2), getPositionYFromTile(2)); // 2 * 8 + 8, 3 * 8 + 8
      hideExcept(SPRITE_DOWN_RIGHT_INDEX);
      return;
   }else {
      hide_sprite(SPRITE_DOWN_RIGHT_INDEX);
   }
   if ((pressedState & J_LEFT) && (pressedState & J_DOWN)) {
      if (isSpriteHidden(SPRITE_DOWN_LEFT_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_01;
      }      
      move_sprite(SPRITE_DOWN_LEFT_INDEX, getPositionXFromTile(0), getPositionYFromTile(2)); // 0 * 8 + 8, 3 * 8 + 8
      hideExcept(SPRITE_DOWN_LEFT_INDEX);
      return;
   } else {
      hide_sprite(SPRITE_DOWN_LEFT_INDEX);
   }
   if ((pressedState & J_LEFT) && (pressedState & J_UP)) {
      if (isSpriteHidden(SPRITE_UP_LEFT_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_01;
      }
      move_sprite(SPRITE_UP_LEFT_INDEX, getPositionXFromTile(0), getPositionYFromTile(0)); // 0 * 8 + 8, 1 * 8 + 8
      hideExcept(SPRITE_UP_LEFT_INDEX);
      return;
   } else {
      hide_sprite(SPRITE_UP_LEFT_INDEX);
   }

   // axis
   if (pressedState & J_UP) {
      if (isSpriteHidden(SPRITE_UP_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_01;
      }
      move_sprite(SPRITE_UP_INDEX, getPositionXFromTile(1), getPositionYFromTile(0));//1 * 8 + 8, 1 * 8 + 8
      return;
   } else {
      hide_sprite(SPRITE_UP_INDEX);
   }
   if (pressedState & J_RIGHT) {
      if (isSpriteHidden(SPRITE_RIGHT_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_01;
      }
      move_sprite(SPRITE_RIGHT_INDEX, getPositionXFromTile(2), getPositionYFromTile(1));//2 * 8 + 8, 2 * 8 + 8
      return;
   } else {
      hide_sprite(SPRITE_RIGHT_INDEX);
   }
   if (pressedState & J_DOWN) {
      if (isSpriteHidden(SPRITE_DOWN_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_01;
      }
      move_sprite(SPRITE_DOWN_INDEX, getPositionXFromTile(1), getPositionYFromTile(2));//1 * 8 + 8, 3 * 8 + 8
      return;
   } else {
      hide_sprite(SPRITE_DOWN_INDEX);
   }
   if (pressedState & J_LEFT) {
      if (isSpriteHidden(SPRITE_LEFT_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_01;
      }
      move_sprite(SPRITE_LEFT_INDEX, getPositionXFromTile(0), getPositionYFromTile(1));//0 * 8 + 8, 2 * 8 + 8
      return;
   } else {
      hide_sprite(SPRITE_LEFT_INDEX);
   }
}

void testAndDisplaySpriteButtons(uint8_t pressedState) {

   // buttons
   if (pressedState & J_A) {
      if (isSpriteHidden(SPRITE_BUTTON_A_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_02;
      }
      move_sprite(SPRITE_BUTTON_A_INDEX, getPositionXFromTile(7), getPositionYFromTile(0));
   } else {
      hide_sprite(SPRITE_BUTTON_A_INDEX);
   }
   if (pressedState & J_B) {
      if (isSpriteHidden(SPRITE_BUTTON_B_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_02;
      }      
      move_sprite(SPRITE_BUTTON_B_INDEX, getPositionXFromTile(6), getPositionYFromTile(1));
   } else {
      hide_sprite(SPRITE_BUTTON_B_INDEX);
   }
   if (pressedState & J_SELECT) {
      if (isSpriteHidden(SPRITE_BUTTON_SEL_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_00;
      }      
      move_sprite(SPRITE_BUTTON_SEL_INDEX, getPositionXFromTile(4), getPositionYFromTile(2));
   } else {
      hide_sprite(SPRITE_BUTTON_SEL_INDEX);
   }
   if (pressedState & J_START) {
      if (isSpriteHidden(SPRITE_BUTTON_STA_INDEX)) {// previously invisible, thus play sound
         CBTFX_PLAY_SFX_00;
      }      
      move_sprite(SPRITE_BUTTON_STA_INDEX, getPositionXFromTile(5), getPositionYFromTile(2));
   } else {
      hide_sprite(SPRITE_BUTTON_STA_INDEX);
   }

}

void main()
{

    font_t min_font;
    /* First, init the font system */
    font_init();
    min_font = font_load(font_min);

    // Enable audio output
    NR52_REG = 0x80;
    NR51_REG = 0xFF;
    NR50_REG = 0x77;
    add_VBL(CBTFX_update);

   DISPLAY_ON;

   // load background tiles
   set_bkg_data(37, sizeof(background_tiles) / 16, background_tiles);
   // load background map
   set_bkg_tiles(0, 0, backgroundWidth, backgroundHeight, background);

   SHOW_BKG;
   SHOW_SPRITES;

    font_set(min_font);
    gotoxy(4, 2);
    printf("Joypad test!");
    gotoxy(1,16);
    printf("2023 Michal Ursiny");

   // load up sprite data
   set_sprite_data(0,sizeof(sprites) / 16, sprites);

   // assign sprite data to sprites
   set_sprite_tile(SPRITE_UP_INDEX, 0);
   set_sprite_tile(SPRITE_UP_RIGHT_INDEX, 2);
   set_sprite_tile(SPRITE_RIGHT_INDEX, 1);

   set_sprite_tile(SPRITE_DOWN_RIGHT_INDEX, 2);
   set_sprite_prop(SPRITE_DOWN_RIGHT_INDEX, S_FLIPY);

   set_sprite_tile(SPRITE_DOWN_INDEX, 0);
   set_sprite_prop(SPRITE_DOWN_INDEX, S_FLIPY);

   set_sprite_tile(SPRITE_DOWN_LEFT_INDEX, 2);
   set_sprite_prop(SPRITE_DOWN_LEFT_INDEX, S_FLIPY | S_FLIPX);

   set_sprite_tile(SPRITE_LEFT_INDEX, 1);
   set_sprite_prop(SPRITE_LEFT_INDEX, S_FLIPX);

   set_sprite_tile(SPRITE_UP_LEFT_INDEX, 2);
   set_sprite_prop(SPRITE_UP_LEFT_INDEX, S_FLIPX);

   set_sprite_tile(SPRITE_BUTTON_A_INDEX, 3);
   set_sprite_tile(SPRITE_BUTTON_B_INDEX, 4);
   set_sprite_tile(SPRITE_BUTTON_STA_INDEX, 6);
   set_sprite_tile(SPRITE_BUTTON_SEL_INDEX, 5);

   while (1)
   {

      uint8_t pressedButtons = joypad();

      CBTFX_update();
      testAndDisplaySpriteDpad(pressedButtons);
      testAndDisplaySpriteButtons(pressedButtons);
      
      wait_vbl_done();
   }
}
