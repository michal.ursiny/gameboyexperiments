/*

		SFX_01

		Sound Effect File.
		
		Info:
			Length			:	14
			Priority		:	0
			Channels used	:	Duty channel 2
		
		This file was generated by hammer2cbt

	*/
	#ifndef __SFX_01_h_INCLUDE
#define __SFX_01_h_INCLUDE
#define CBTFX_PLAY_SFX_01 CBTFX_init(&SFX_01[0])
extern const unsigned char SFX_01[];
#endif