/*

		SFX_00

		Sound Effect File.
		
		Info:
			Length			:	14
			Priority		:	0
			Channels used	:	Duty channel 2 & Noise channel
		
		This file was generated by hammer2cbt

	*/
	const unsigned char SFX_00[] = {
240, // Header
14,
1, 170, 32, 255, 196, 134, 80, 0, 170, 64, 248, 158, 134, 48, 0, 170, 64, 255, 114, 134, 112, 0, 170, 128, 228, 66, 134, 48, 0, 170, 128, 210, 10, 134, 32, 1, 170, 128, 193, 237, 133, 16, 1, 34, 128, 176, 172, 133, 0, 1, 32, 32, 64, 196, 134, 0, 0, 32, 64, 64, 158, 134, 0, 0, 32, 64, 64, 114, 134, 0, 0, 2, 128, 64, 66, 134, 0, 0, 2, 128, 48, 10, 134, 0, 1, 2, 128, 32, 237, 133, 0, 1, 2, 128, 16, 172, 133, 0
};